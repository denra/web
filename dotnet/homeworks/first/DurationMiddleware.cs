using System;
using static System.Math;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
 
public class DurationMiddleware
{
    private readonly RequestDelegate _next;
 
    public DurationMiddleware(RequestDelegate next)
    {
        this._next = next;
    }

    private long GetUnixTime() 
    {
        return DateTimeOffset.Now.ToUnixTimeSeconds();
    }
 
    public async Task InvokeAsync(HttpContext context)
    {
        // var token = context.Request.Query["token"];
        // if (token!="12345678")
        // {
        //     context.Response.StatusCode = 403;
        //     await context.Response.WriteAsync("Token is invalid");
        // }
        // else
        // {
            // Замер времени
            var start = GetUnixTime();
            await _next.Invoke(context);
            var end = GetUnixTime();
            var duration = end - start;

            // Форматирование вывода
            var startLine = "Начало обработки запроса: " + start;
            var endLine =   "Конец обработки запроса: " + end;
            var durationLine =  "Длительность обработки запроса: " + duration + " мс";

            var maxLen = Max(Max(startLine.Length, endLine.Length), durationLine.Length);
            var separatorLine = "";
            var subSeparatorLine = "";
            for (int i = 0; i < maxLen; i++) {
                separatorLine += "=";
                subSeparatorLine += "-";
            }

            // Вывод
            System.Console.WriteLine(separatorLine);
            System.Console.WriteLine("UseMiddleware");
            System.Console.WriteLine(subSeparatorLine);

            System.Console.WriteLine(startLine);
            System.Console.WriteLine(endLine);
            System.Console.WriteLine(durationLine);

            System.Console.WriteLine(separatorLine);
            
        // }
    }
}