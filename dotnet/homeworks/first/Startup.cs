﻿using System;
using static System.Math;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace test_room
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Use((context, next) => {
                Func<long> getUnixTime = () => {return DateTimeOffset.Now.ToUnixTimeSeconds();};

                // Замер времени
                var start = getUnixTime();
                next();
                var end = getUnixTime();
                var duration = end - start;

                // Форматирование вывода
                var startLine = "Начало обработки запроса: " + start;
                var endLine =   "Конец обработки запроса: " + end;
                var durationLine =  "Длительность обработки запроса: " + duration + " мс";

                var maxLen = Max(Max(startLine.Length, endLine.Length), durationLine.Length);
                var separatorLine = "";
                var subSeparatorLine = "";
                
                for (int i = 0; i < maxLen; i++) {
                    separatorLine += "=";
                    subSeparatorLine += "-";
                }

                // Вывод
                System.Console.WriteLine(separatorLine);
                System.Console.WriteLine("Use");
                System.Console.WriteLine(subSeparatorLine);

                System.Console.WriteLine(startLine);
                System.Console.WriteLine(endLine);
                System.Console.WriteLine(durationLine);

                System.Console.WriteLine(separatorLine);

                return Task.Delay(0);
            });

            app.UseMiddleware<DurationMiddleware>();

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
